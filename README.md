# Portefeuille

## Auteurs
- Deneudt Lucas
- Duvent Léo

## Présentation
Application de gestion de cryptomonnaies. Fonctionnalités :
- Création de compte utilisateur
- Connexion à son compte utilisateur
- Gestion de son profil
- Ajout de transactions de cryptomonnaies
- Suppression de transactions de cryptomonnaies
- Visualisation des informations principales sur un dashboard

### Api de cryptomonnaies
L'api "CoinGecko" a été utilisé afin de récupérer des informations sur les cryptomonnaies.

## Plugins Front
- VueJs
- VueRouter
- Vuex
- VuePersistedState
- Vuetify
- Axios
- ChartJs / VueChartJs 

### Back
A noter qu'un module "Vuex" a été créé dans le but de réaliser une base de données fictive. L'application fonctionne de manière autonome afin de réaliser des tests sur le front.

## Project setup
```
nvm use 16
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
