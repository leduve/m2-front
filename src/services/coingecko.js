import axios from "axios";

const instance = axios.create({
  baseURL: "https://api.coingecko.com/api/v3/",
  headers: { accept: "application/json" },
});

async function retrieveCoins() {
  let coinsList = [];
  await instance
    .get("/coins/list")
    .then(function (response) {
      coinsList = response.data
        .map((data) => {
          return data;
        });
    })
    .catch(function (error) {
      console.error(error);
    });
  return coinsList;
}

async function retrieveCoinDetail(coinId) {
  try {
    const response = await instance.get(`/coins/${coinId}`, {
      params: {
        sparkline: true
      }
    });
    return response;
  } catch (error) {
    console.error(`Something wrong when try to get coin detail : ${error}`);
  }
}

async function retrieveCoinDetailAtDate(coinId, date) {
  try {
    const response = await instance.get(`/coins/${coinId}/history`, {
      params: {
        date: date
      }
    });
    return response;
  } catch (error) {
    console.error(`Something wrong when try to get coin detail : ${error}`);
  }
}


export { retrieveCoins, retrieveCoinDetail, retrieveCoinDetailAtDate};
