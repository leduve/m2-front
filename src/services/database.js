import store from "@/store";

function login(userEmail, password) {
  let users = store.getters["database/users"];
  let user = users.find((user) => user.email === userEmail);
  if (user !== undefined) {
    return user.password === password;
  }
  return false;
}

function getUser(userEmail) {
  let users = store.getters["database/users"];
  return users.find((user) => user.email === userEmail);
}

function register(email, firstName, lastName, password) {
  store.commit("database/addUser", { email, firstName, lastName, password });
}

function update(oldEmail, email, firstName, lastName, password) {
  store.commit("database/updateUser", {
    oldEmail,
    email,
    firstName,
    lastName,
    password,
  });
}

function addCurrency(email, quantity, currency) {
  store.commit("database/addMoneyToUser", { email, quantity, currency });
}

function getWallet(email) {
  let user = getUser(email);
  return user.wallet;
}

export { login, getUser, register, getWallet, update, addCurrency };
