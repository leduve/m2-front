import store from "@/store";

function update(oldEmail, firstName, lastName, email, password) {
  store.commit("database/updateUser", {
    oldEmail: oldEmail,
    firstName: firstName,
    lastName: lastName,
    email: email,
    password: password
  })
  return true;
}

export { update };
