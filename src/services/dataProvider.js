import { retrieveCoinDetail, retrieveCoinDetailAtDate, retrieveCoins} from "@/services/coingecko";
import store from "@/store";
import dateFormat from "dateformat";

function updateWalletMoneyData() {
    const myMoney = store.getters["wallet/myMoney"];
    myMoney.forEach((currency) => {
        const detail = retrieveCoinDetail(currency.name);
        detail.then((response) => {
            store.commit("wallet/addPriceChangePercentage24hInCurrency", {
                id: response.data.id,
                eur: response.data.market_data
                    .price_change_percentage_24h_in_currency.eur,
            });
            store.commit("wallet/addCurrentPrice", {
                id: response.data.id,
                eur: response.data.market_data.current_price.eur,
            });
            store.commit("wallet/addpriceChangePercentage7dInCurrency", {
                id: currency.name,
                seven_days: response.data.market_data.sparkline_7d.price,
            });
        });
    });
    let sumLocal = 0;
    for (let money of myMoney) {
        for (let item of money.items) {
            sumLocal += item.totalEur
        }
    }
    let sumOnline = 0
    for (let money of myMoney) {
        for (let item of money.items) {
            retrieveCoinDetailAtDate(money.name, dateFormat(new Date(Date.now()), "dd-mm-yyyy")).then((response => {
                if (response != null && response.data != null) {
                    sumOnline += response.data.market_data.current_price.eur * item.quantity
                    store.commit("wallet/setProfit", {
                        profit: sumLocal - sumOnline
                    })
                }
            }));
        }
    }

    retrieveCoins()
        .then((currencies) => {
            store.commit("wallet/setCurrencies", {
                currencies: currencies
            })
        })
}

export default updateWalletMoneyData;