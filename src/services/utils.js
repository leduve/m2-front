function color(percentage) {
    if (percentage > 0) {
        return "#baf89c"
    } else if (percentage === 0) {
        return "primary"
    } else {
        return "#ff6363"
    }
}

export {color}