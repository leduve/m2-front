import store from '@/store/index.js'
import {login as databaseLogin, getUser, register as databaseRegister} from '@/services/database.js'

function login(email, password) {
    if (databaseLogin(email, password)) {
        let user = getUser(email)
        store.commit("user/setFirstName", user.firstName);
        store.commit("user/setLastName", user.lastName);
        store.commit("user/setEmail", user.email);
        store.commit("user/setPassword", user.password);
        return true;
    } else {
        return false;
    }
}

function register(email, firstName, lastName, password) {
    databaseRegister(email, firstName, lastName, password);
    return true;
}

export {login, register};
