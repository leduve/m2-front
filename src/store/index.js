import Vue from "vue";
import Vuex from "vuex";
import { user } from "@/store/user.js";
import { navigation } from "@/store/navigation.js";
import { wallet } from "@/store/wallet";
import { database } from "@/store/database";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    user,
    navigation,
    wallet,
    database
  },
  plugins: [
    createPersistedState({
      paths: ["database"]
    })
  ]
});
