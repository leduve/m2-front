export const database = {
  namespaced: true,
  state: () => ({
    users: [
      {
        firstName: "Lucas",
        lastName: "Deneudt",
        email: "deneudtlucas@gmail.com",
        password: "admin",
        wallet: [
          {
            name: "green-satoshi-token",
            total: 168,
            items: [
              {
                quantity: "100",
                eur: 1.68,
                totalEur: 168,
                date: 1653760727691,
              },
            ],
          },
          {
            name: "1x-short-dogecoin-token",
            total: 72.5204,
            items: [
              {
                quantity: "100",
                eur: 0.725204,
                totalEur: 72.5204,
                date: 1653762264537,
              },
            ],
          },
        ],
      },
      {
        firstName: "Léo",
        lastName: "Duvent",
        email: "admin",
        password: "admin",
        wallet: [
          {
            name: "bitcoin",
            total: 107.776,
            items: [
              {
                quantity: "0.004",
                eur: 26944,
                totalEur: 107.776,
                date: 1653760187205,
              },
            ],
          },
          {
            name: "ethereum",
            total: 8.2786,
            items: [
              {
                quantity: "0.005",
                eur: 1655.72,
                totalEur: 8.2786,
                date: 1653760292619,
              },
            ],
          },
        ],
      },
    ],
  }),
  mutations: {
    addUser(state, values) {
      state.users.push({
        firstName: values.firstName,
        lastName: values.lastName,
        email: values.email,
        password: values.password,
        wallet: [],
      });
    },
    updateUser(state, values) {
      let user = state.users.find((user) => user.email === values.oldEmail);
      if (user != null) {
        user.email = values.email;
        user.password = values.password;
        user.firstName = values.firstName;
        user.lastName = values.lastName;
      }
    },
    addMoneyToUser(state, values) {
      let user = state.users.find((user) => user.email === values.email);
      let wallet = user.wallet.find(
        (walletUnit) => walletUnit.name === values.currency.id
      );
      if (wallet !== undefined) {
        wallet.total =
          wallet.total +
          values.quantity * values.currency.market_data.current_price.eur;
        wallet.items.push({
          quantity: values.quantity,
          eur: values.currency.market_data.current_price.eur,
          totalEur:
            values.quantity * values.currency.market_data.current_price.eur,
          date: Date.now(),
        });
      } else {
        user.wallet.push({
          name: values.currency.id,
          total:
            values.quantity * values.currency.market_data.current_price.eur,
          items: [],
        });

        let wallet = user.wallet.find(
          (walletUnit) => walletUnit.name === values.currency.id
        );

        wallet.items.push({
          quantity: values.quantity,
          eur: values.currency.market_data.current_price.eur,
          totalEur:
            values.quantity * values.currency.market_data.current_price.eur,
          date: Date.now(),
        });
      }
    },
    deleteCurrencyFromUser(state, values) {
      let user = state.users.find((user) => user.email === values.email);
      const removeIndex = user.wallet.findIndex(
        (walletUnit) => walletUnit.name === values.name
      );
      user.wallet.splice(removeIndex, 1);
    },
    updateMoneyFromUser(state, values) {
      state.users
        .find((user) => user.email === values.email)
        .wallet.remove(values.name === name && values.total === 0);
    },
  },
  actions: {},
  getters: {
    users(state) {
      return state.users;
    },
  },
};
