export const user = {
  namespaced: true,
  state: () => ({
    firstName: null,
    lastName: null,
    email: null,
    password: null,
    isLogged: false,
  }),
  mutations: {
    setFirstName(state, value) {
      state.firstName = value;
    },
    setLastName(state, value) {
      state.lastName = value;
    },
    setEmail(state, value) {
      state.email = value;
    },
    setPassword(state, value) {
      state.password = value;
    },
    setIsLogged(state, value) {
      state.isLogged = value;
    },
  },
  actions: {},
  getters: {
    firstName(state) {
      return state.firstName;
    },
    lastName(state) {
      return state.lastName;
    },
    email(state) {
      return state.email;
    },
    password(state) {
      return state.password;
    },
    isLogged(state) {
      return state.isLogged;
    },
  },
};
