import router from "../router";

export const navigation = {
  namespaced: true,
  state: () => ({
    selectedItem: 0,
    items: [
      { text: "Dashboard", icon: "mdi-view-dashboard-outline", path: "/" },
      {
        text: "My investments",
        icon: "mdi-cash-multiple",
        path: "/investments",
      },
      { text: "Settings", icon: "mdi-cog-outline", path: "/settings" },
    ],
  }),
  mutations: {
    setSelectedItem(state, value) {
      state.selectedItem = value;
    },
  },
  actions: {
    setNewPath({ getters }) {
      router.push(getters.items[getters.selectedItem].path);
    },
  },
  getters: {
    selectedItem(state) {
      return state.selectedItem;
    },
    items(state) {
      return state.items;
    },
  },
};
