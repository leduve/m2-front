export const wallet = {
  namespaced: true,
  state: () => ({
    myMoney: [],
    priceChangePercentage24hInCurrency: [],
    totalPriceChangePercentage24hInCurrencies: 0,
    priceChangePercentage7dInCurrency: [],
    currentPrice: [],
    currencies: [],
    profit: 0,
  }),
  mutations: {
    setMyMoney(state, value) {
      state.myMoney = value;
    },
    addPriceChangePercentage24hInCurrency(state, value) {
      state.priceChangePercentage24hInCurrency.push({
        id: value.id,
        eur: value.eur,
      });
      state.totalPriceChangePercentage24hInCurrencies += value.eur;
    },
    addpriceChangePercentage7dInCurrency(state, value) {
      state.priceChangePercentage7dInCurrency.push({
        id: value.id,
        seven_days: value.seven_days,
      });
    },
    addCurrentPrice(state, value) {
      state.currentPrice.push({
        id: value.id,
        eur: value.eur,
      });
    },
    setProfit(state, value) {
      state.profit = value.profit
    },
    setCurrencies(state, value) {
      state.currencies = value.currencies;
    }
  },
  actions: {},
  getters: {
    myMoney(state) {
      return state.myMoney;
    },
    priceChangePercentage24hInCurrency(state) {
      return state.priceChangePercentage24hInCurrency;
    },
    priceChangePercentage7dInCurrency(state) {
      return state.priceChangePercentage7dInCurrency;
    },
    currentPrice(state) {
      return state.currentPrice;
    },
    totalPriceChangePercentage24hInCurrencies(state) {
      return state.totalPriceChangePercentage24hInCurrencies;
    },
    profit(state) {
      return state.profit;
    },
    currencies(state) {
      return state.currencies;
    }
  },
};
