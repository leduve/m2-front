import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import store from './store'
import { preset } from 'vue-cli-plugin-vuetify-preset-reply/preset'

Vue.config.productionTip = false

new Vue({
  preset,
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
